core = 7.x
api = 2

; uw_strategic_plan_2020
projects[uw_strategic_plan_2020][type] = "module"
projects[uw_strategic_plan_2020][download][type] = "git"
projects[uw_strategic_plan_2020][download][url] = "https://git.uwaterloo.ca/MSI/uw_strategic_plan_2020.git"
projects[uw_strategic_plan_2020][download][tag] = "7.x-1.6"


